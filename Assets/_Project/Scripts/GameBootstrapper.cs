using _Project.Scripts.Core_Logic.StatesMachine;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Loading;
using _Project.Scripts.Services.SaveSystem;
using UnityEngine;

namespace _Project.Scripts
{
  public class GameBootstrapper : MonoBehaviour
  {
    private IGameStateMachine _gameStateMachine;

    private IAssetProvider _assetProvider;
    private IAudioService _audioService;
    private IGameSaveSystem _gameSaveSystem;
    private ILoadingService _loadingService;

    private void Awake()
    {
      Application.targetFrameRate = 60;

      if (!Application.isEditor)
        Debug.unityLogger.logEnabled = false;

      CreateServices();
      CreateGameStateMachine();
      LaunchStartupState();
    }

    private void CreateServices()
    {
      _assetProvider = new AssetProvider();
      _audioService = new AudioService(_assetProvider);
      _gameSaveSystem = new GameSaveSystem();
      _loadingService = new LoadingService(_assetProvider);
    }

    private void CreateGameStateMachine()
    {
      var loadHomeState = new LoadHomeState(_assetProvider, _gameSaveSystem, _audioService, _loadingService);
      var loadStartupState = new LoadSplashState(_audioService, _gameSaveSystem, _assetProvider, _loadingService);
      _gameStateMachine = new GameStateMachine(loadStartupState, loadHomeState);
    }


    private void LaunchStartupState() =>
      _gameStateMachine.Enter<LoadSplashState>();
  }
}