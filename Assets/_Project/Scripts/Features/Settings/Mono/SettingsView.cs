using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Settings.Mono
{
  public class SettingsView : MonoBehaviour
  {
    public Button ButtonBack;

    public Button ButtonMusic;
    public Button ButtonSound;

    public Button Screen;
    
    public Image IconMusic;
    public Image IconSound;

    public RectTransform PopupRectTransform;
  }
}