﻿using System;
using System.Collections.Generic;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.Settings.Mono;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using UnityEngine;
using AudioType = _Project.Scripts.Services.Audio.Enum.AudioType;

namespace _Project.Scripts.Features.Settings
{
  public class SettingsLogic
  {
    private const string IconMusicOnPath = "icon_music_on";
    private const string IconMusicOffPath = "icon_music_off";
    private const string IconSoundOnPath = "icon_sound_on";
    private const string IconSoundOffPath = "icon_sound_off";
    
    private const float EndPosition = 200f;

    private readonly IAudioService _audioService;
    private readonly IAssetProvider _assetProvider;
    private readonly IGameSaveSystem _gameSaveSystem;

    private readonly SettingsView _settingsView;

    private Sprite _musicOn;
    private Sprite _musicOff;
    private Sprite _soundOn;
    private Sprite _soundOff;

    private SlidePopup _slidePopup;
    private List<FadeCanvasGroup> _fadeCanvasGroups;

    private string _cachedScreen;

    public SettingsLogic(SettingsView settingsView, IGameSaveSystem gameSaveSystem, IAudioService audioService, IAssetProvider assetProvider)
    {
      _audioService = audioService;
      _assetProvider = assetProvider;
      _gameSaveSystem = gameSaveSystem;

      _settingsView = settingsView;
    }

    public void Construct()
    {
      AssignFades();
      LoadSprites();
      AssignListeners();

      MuteMusic(_gameSaveSystem.Get().IsMusicDisabled);
      MuteSounds(_gameSaveSystem.Get().IsSoundDisabled);

      AddButtonSound();

      new ButtonAnimation(_settingsView.ButtonBack.transform);

      _slidePopup = new SlidePopup(_settingsView.PopupRectTransform);
      _settingsView.gameObject.SetActive(false);
    }

    public void OpenWindow()
    {
      _slidePopup.Open(null, null, EndPosition);
      _fadeCanvasGroups.ForEach(x => x.Appear());
      
      _settingsView.gameObject.SetActive(true);
    }

    public void Clear()
    {
    }

    private void AddButtonSound()
    {
      new ButtonSound(_audioService, _settingsView.ButtonMusic.gameObject, AudioId.Click);
      new ButtonSound(_audioService, _settingsView.ButtonSound.gameObject, AudioId.Click);
      new ButtonSound(_audioService, _settingsView.ButtonBack.gameObject, AudioId.ClickBack);
    }

    private void CloseWindow()
    {
      _fadeCanvasGroups.ForEach(x => x.Disappear());
      _slidePopup.Close(null, () => _settingsView.gameObject.SetActive(false));
    }

    private void Back() =>
      CloseWindow();

    private void SwitchMusic() =>
      MuteMusic(!_audioService.MusicIsMute());

    private void SwitchSound() =>
      MuteSounds(!_audioService.SoundsIsMute());

    private void AssignListeners()
    {
      _settingsView.Screen.onClick.AddListener(Back);
      _settingsView.ButtonBack.onClick.AddListener(Back);
      _settingsView.ButtonMusic.onClick.AddListener(SwitchMusic);
      _settingsView.ButtonSound.onClick.AddListener(SwitchSound);
    }

    private void MuteMusic(bool mute)
    {
      SwitchAudio(AudioType.Music, mute);

      _gameSaveSystem.Get().IsMusicDisabled = mute;
      _gameSaveSystem.Save();
    }

    private void MuteSounds(bool mute)
    {
      SwitchAudio(AudioType.Sound, mute);

      _gameSaveSystem.Get().IsSoundDisabled = mute;
      _gameSaveSystem.Save();
    }
    
    private void AssignFades()
    {
      _fadeCanvasGroups = new List<FadeCanvasGroup>();
      foreach (CanvasGroup canvasGroup in _settingsView.GetComponentsInChildren<CanvasGroup>(true))
        _fadeCanvasGroups.Add(new FadeCanvasGroup(canvasGroup));
    }

    private void SwitchAudio(AudioType type, bool mute)
    {
      switch (type)
      {
        case AudioType.Music:
          _settingsView.IconMusic.sprite = mute ? _musicOff : _musicOn;
          _audioService.MuteMusic(mute);
          break;
        case AudioType.Sound:
          _settingsView.IconSound.sprite = mute ? _soundOff : _soundOn;
          _audioService.MuteSounds(mute);
          break;
        default:
          throw new ArgumentOutOfRangeException(nameof(type), type, null);
      }
    }

    private void LoadSprites()
    {
      _musicOn = LoadSprite(IconMusicOnPath);
      _musicOff = LoadSprite(IconMusicOffPath);
      _soundOn = LoadSprite(IconSoundOnPath);
      _soundOff = LoadSprite(IconSoundOffPath);
    }

    private Sprite LoadSprite(string path) =>
      _assetProvider.GetResource<Sprite>(path);
  }
}