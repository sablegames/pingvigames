using _Project.Scripts.Features.Settings.Mono;

namespace _Project.Scripts.Features
{
  public interface IUiFactory
  {
    SettingsView SettingsView { get; }

    void CreateSettingsView();
    void ClearSettingsView();
  }
}