using System;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;

namespace _Project.Scripts.Features.Splashscreen
{
  public class SplashLogic
  {
    private readonly IAudioService _audioService;
    private readonly ISplashFactory _splashFactory;
    private readonly Splash _splash;

    public SplashLogic(IAssetProvider assetProvider, IAudioService audioService)
    {
      _audioService = audioService;
      _splashFactory = new SplashFactory(assetProvider);
      _splash = _splashFactory.CreateSplash();
    }

    public void Launch(Action onEnd)
    {
      _splash.Construct(_audioService,() => onEnd?.Invoke());
      _splash.Launch();
    }

    public void Clear() => 
      _splashFactory.Clear();
  }
}