﻿using System;
using _Project.Scripts.Animation;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.Splashscreen
{
  public class Splash : MonoBehaviour
  {
    private const float Delay = .2f;
    private const float EndPosition = 100f;

    public Button StartButton;
    public RectTransform PopupTransform;
    public CanvasGroup PopupCanvasGroup;

    private Action _onSplashComplete;

    private SlidePopup _slidePopup;
    private FadeCanvasGroup _fadeCanvasGroup;

    public void Construct(IAudioService audioService, Action onClickStart)
    {
      _onSplashComplete = onClickStart;
      StartButton.onClick.AddListener(OnClickStart);

      new ButtonAnimation(StartButton.transform);
      new ButtonSound(audioService, StartButton.gameObject, AudioId.Click);

      _slidePopup = new SlidePopup(PopupTransform);
      _fadeCanvasGroup = new FadeCanvasGroup(PopupCanvasGroup);
    }

    public void Launch()
    {
      _slidePopup.Open(null,null, EndPosition);
      _fadeCanvasGroup.Appear(null,null, Delay);
    }

    private void OnClickStart()
    {
      _slidePopup.Close(null, () =>
      {
        _fadeCanvasGroup.Clear();
        _onSplashComplete?.Invoke();
      });
      _fadeCanvasGroup.Disappear();
    }
  }
}
