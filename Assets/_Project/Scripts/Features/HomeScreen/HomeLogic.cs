using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using _Project.Scripts.Features.Settings;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Loading;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeLogic
  {
    private readonly IHomeFactory _homeFactory;
    private readonly IUiFactory _uiFactory;

    private readonly IAudioService _audioService;
    private readonly IAssetProvider _assetProvider;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ILoadingService _loadingService;
    private readonly IGameStateMachine _gameStateMachine;
    
    private SettingsLogic _settingsLogic;
    
    public HomeLogic(IGameStateMachine gameStateMachine, IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem, IAudioService audioService,
      ILoadingService loadingService)
    {
      _audioService = audioService;
      _assetProvider = assetProvider;
      _gameSaveSystem = gameSaveSystem;
      _loadingService = loadingService;
      _gameStateMachine = gameStateMachine;

      _uiFactory = new UiFactory(assetProvider);
      _homeFactory = new HomeFactory(assetProvider, audioService);
    }
    
    public void Enter()
    {
      CreateHomeView();
      CreateSettings();
    }

    public void CloseHomeView()
    {
      _settingsLogic.Clear();
      _homeFactory.ClearHomeView();
      _uiFactory.ClearSettingsView();
    }

    private void CreateHomeView()
    {
      _homeFactory.CreateHomeView();
      _homeFactory.HomeViewLogic.Construct(OpenSettingsWindow, Restart);
    }

    private void CreateSettings()
    {
      _uiFactory.CreateSettingsView();
      
      _settingsLogic = new SettingsLogic(_uiFactory.SettingsView, _gameSaveSystem, _audioService, _assetProvider);
      _settingsLogic.Construct();
    }

    private void Restart() => 
      _loadingService.Enter(_gameStateMachine.Enter<LoadHomeState>);

    private void OpenSettingsWindow() =>
      _settingsLogic.OpenWindow();
  }
}