using _Project.Scripts.Core_Logic.Common;
using _Project.Scripts.Features.HomeScreen.Mono;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using UnityEngine;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeFactory : GameObjectFactory, IHomeFactory
  {
    private const string BaseCanvas = "Canvas";
    
    private const string HomeViewPath = "Features/HomeScreen/Home";

    private HomeViewMono HomeViewMono { get; set; }
    public IHomeViewLogic HomeViewLogic { get; private set; }

    private readonly IAudioService _audioService;
    
    public HomeFactory(IAssetProvider assetProvider, IAudioService audioService) : base(assetProvider) => 
      _audioService = audioService;

    public void CreateHomeView()
    {
      ClearHomeView();
      HomeViewMono = Create<HomeViewMono>(HomeViewPath, GameObject.Find(BaseCanvas).transform);
      HomeViewLogic = new HomeViewLogic(HomeViewMono, _audioService);
    }

    public void ClearHomeView()
    {
      if (!HomeViewMono) return;
      
      Object.Destroy(HomeViewMono.gameObject);
    }
  }
}