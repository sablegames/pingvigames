using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.HomeScreen.Mono
{
  public class HomeViewMono : MonoBehaviour
  {
    public Button RestartButton;
    public Button SettingsButton;
  }
}