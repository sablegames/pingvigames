using System;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.HomeScreen.Mono;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeViewLogic : IHomeViewLogic
  {
    private readonly HomeViewMono _homeViewMono;
    private readonly IAudioService _audioService;

    public HomeViewLogic(HomeViewMono homeViewMono, IAudioService audioService)
    {
      _homeViewMono = homeViewMono;
      _audioService = audioService;
    }

    public void Construct(Action openSettingsWindow, Action toHome)
    {
      _homeViewMono.RestartButton.onClick.AddListener(toHome.Invoke);
      _homeViewMono.SettingsButton.onClick.AddListener(openSettingsWindow.Invoke);

      CreateAnimations();
      CreateSounds();
    }

    private void CreateAnimations()
    {
      new ButtonAnimation(_homeViewMono.RestartButton.transform);
      new ButtonAnimation(_homeViewMono.SettingsButton.transform);
    }

    private void CreateSounds()
    {
      new ButtonSound(_audioService, _homeViewMono.RestartButton.gameObject, AudioId.Click);
      new ButtonSound(_audioService, _homeViewMono.SettingsButton.gameObject, AudioId.Click);
    }
  }
}