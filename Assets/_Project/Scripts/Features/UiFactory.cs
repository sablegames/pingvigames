using _Project.Scripts.Core_Logic.Common;
using _Project.Scripts.Features.Settings.Mono;
using _Project.Scripts.Services.AssetProvider;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Features
{
  public class UiFactory : GameObjectFactory, IUiFactory
  {
    private const string SettingsPath = "Features/Settings/SettingsView";

    private const string BaseCanvas = "Canvas";

    public SettingsView SettingsView { get; private set;}

    public UiFactory(IAssetProvider assetProvider) : base(assetProvider) {}

    public void CreateSettingsView()
    {
      Transform parent = GameObject.Find(BaseCanvas).transform;
      SettingsView = Create<SettingsView>(SettingsPath, parent);
    }

    public void ClearSettingsView()
    {
      if(SettingsView)
        Object.Destroy(SettingsView.gameObject);
    }
  }
}