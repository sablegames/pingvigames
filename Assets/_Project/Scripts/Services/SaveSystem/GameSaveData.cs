namespace _Project.Scripts.Services._data
{
  public class GameSaveData
  {
    public bool IsPolicyAccepted;
    
    public bool IsMusicDisabled;
    public bool IsSoundDisabled;
    public bool IsHintsDisabled;
    
    public uint MoneyCount = 100;
    
    // public List<string> UnlockedShopContent = new ();
  }
}