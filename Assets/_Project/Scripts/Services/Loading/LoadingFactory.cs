using System;
using _Project.Scripts.Core_Logic.Common;
using _Project.Scripts.Services.AssetProvider;

namespace _Project.Scripts.Services.Loading
{
  public class LoadingFactory : GameObjectFactory, ILoadingFactory
  {
    private const string LoadingPath = "Features/Loading/Loading";

    private LoadingMono LoadingMono { get; set; }

    public LoadingFactory(IAssetProvider assetProvider) : base(assetProvider)
    { }

    public void CreateScreen(Action onLoad)
    {
      ClearScreen();
      LoadingMono = Create<LoadingMono>(LoadingPath);
      LoadingMono.Construct(onLoad, ClearScreen);
    }

    public void ClearScreen()
    {
      if (LoadingMono == null)
        return;
      
      Clear(LoadingMono.gameObject);
      LoadingMono = null;
    }
  }
}