using System;

namespace _Project.Scripts.Services.Loading
{
  public interface ILoadingService
  {
    void Enter(Action onLoad);
    void Exit();
  }
}