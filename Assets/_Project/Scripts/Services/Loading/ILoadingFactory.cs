using System;

namespace _Project.Scripts.Services.Loading
{
  public interface ILoadingFactory
  {
    void CreateScreen(Action onLoad);
    void ClearScreen();
  }
}