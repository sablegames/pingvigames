using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Services.Loading
{
  public class LoadingMono : MonoBehaviour
  {
    private const float Time = 3f;
    private const float FadeDuration = .5f;

    public Transform Icon;
    public CanvasGroup CanvasGroup;

    private Tweener _tweener;
    
    public void Construct(Action onLoad, Action onEnd) =>
      Animate(onLoad, onEnd);

    private void Animate(Action onLoad, Action onEnd)
    {
      AnimateIcon();
      ShowLoading(onLoad);
      WaitLoading(onEnd);
    }

    private void WaitLoading(Action onEnd)
    {
      var startValue = 0f;
      _tweener = DOTween.To(() => startValue, x => startValue = x, 1f, Time)
        .SetEase(Ease.Linear)
        .OnComplete(() => Hide(onEnd));
    }

    private void ShowLoading(Action onLoad)
    {
      CanvasGroup.alpha = 0;
      CanvasGroup.blocksRaycasts = true;
      CanvasGroup.DOFade(1, FadeDuration)
        .OnComplete(() => onLoad?.Invoke());
    }

    private void AnimateIcon()
    {
      Icon.DOLocalRotate(Vector3.back * 360, 1f, RotateMode.FastBeyond360)
        .SetEase(Ease.Linear)
        .SetLoops(-1, LoopType.Restart);
    }

    private void Hide(Action onEnd)
    {
      CanvasGroup.DOFade(0, FadeDuration)
        .OnComplete(() =>
        {
          OnHide();
          onEnd?.Invoke();
        });
    }

    private void OnHide()
    {
      CanvasGroup.blocksRaycasts = false;
      Icon.DOKill();
      CanvasGroup.DOKill();
      _tweener.Kill();
    }
  }
}