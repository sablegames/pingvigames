using System;
using _Project.Scripts.Services.AssetProvider;

namespace _Project.Scripts.Services.Loading
{
  public class LoadingService : ILoadingService
  {
    private readonly ILoadingFactory _loadingFactory;

    public LoadingService(IAssetProvider assetProvider) => 
      _loadingFactory = new LoadingFactory(assetProvider);

    public void Enter(Action onLoad) => 
      _loadingFactory.CreateScreen(onLoad);

    public void Exit() => 
      _loadingFactory.ClearScreen();
  }
}