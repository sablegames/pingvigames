using System;
using System.Threading.Tasks;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Features.Splashscreen;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Loading;

namespace _Project.Scripts.Core_Logic.StatesMachine.States
{
  public class LoadSplashState : IState
  {
    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ILoadingService _loadingService;
    
    private IGameStateMachine _gameStateMachine;
    private SplashLogic _splashLogic;

    public LoadSplashState(IAudioService audioService, IGameSaveSystem gameSaveSystem, IAssetProvider assetProvider, ILoadingService loadingService)
    {
      _assetProvider = assetProvider;
      _audioService = audioService;
      _gameSaveSystem = gameSaveSystem;
      _loadingService = loadingService;
    }

    public void Construct(IGameStateMachine gameStateMachine) =>
      _gameStateMachine = gameStateMachine;

    public void Enter()
    {
      LaunchWithSplash(LoadHome);
      LoadSound();
    }

    public Task EnterAsync() =>
      throw new NotImplementedException();

    public void Exit() { }

    private void LaunchWithSplash(Action onSplashComplete)
    {
      _splashLogic = new SplashLogic(_assetProvider, _audioService);
      _splashLogic.Launch(onSplashComplete);
    }

    private void LoadHome()
    {
      _loadingService.Enter(() =>
      {
        _splashLogic.Clear();
        _gameStateMachine.Enter<LoadHomeState>();
      });
    }
    
    private void LoadSound()
    {
      _audioService.MuteMusic(_gameSaveSystem.Get().IsMusicDisabled);
      _audioService.MuteSounds(_gameSaveSystem.Get().IsSoundDisabled);
      
      _audioService.Play(AudioId.Music_OST_Standart);
    }
  }
}