using System.Threading.Tasks;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Features.HomeScreen;
using _Project.Scripts.Services._Interfaces;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Loading;

namespace _Project.Scripts.Core_Logic.StatesMachine.States
{
  public class LoadHomeState : IState
  {
    private readonly IAudioService _audioService;
    private readonly IAssetProvider _assetProvider;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ILoadingService _loadingService;

    private IGameStateMachine _gameStateMachine;

    private HomeLogic _homeLogic;

    public LoadHomeState(IAssetProvider assetProvider, IGameSaveSystem gameSaveSystem, IAudioService audioService, ILoadingService loadingService)
    {
      _audioService = audioService;
      _assetProvider = assetProvider;
      _loadingService = loadingService;
      _gameSaveSystem = gameSaveSystem;
    }

    public void Construct(IGameStateMachine gameStateMachine) =>
      _gameStateMachine = gameStateMachine;

    public void Enter()
    {
      _homeLogic = new HomeLogic(_gameStateMachine, _assetProvider, _gameSaveSystem, _audioService, _loadingService);
      _homeLogic.Enter();
    }

    public Task EnterAsync() => 
      throw new System.NotImplementedException();

    public void Exit() =>
      _homeLogic.CloseHomeView();
  }
}