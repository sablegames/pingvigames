﻿using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace _Project.Scripts.Animation
{
  public class FadeCanvasGroup
  {
    private const float Duration = 0.5f;
    private const float StartValue = 0f;
    private const float EndValue = 1f;
    
    private readonly CanvasGroup _canvasGroup;

    private Tweener _tweenAppear;
    private Tweener _tweenDisappear;

    public FadeCanvasGroup(CanvasGroup canvasGroup)
    {
      var objectWithAnimation = canvasGroup.gameObject.AddComponent<ObjectWithAnimation>();
      objectWithAnimation.Construct(() =>
      {
        _tweenAppear?.Kill();
        _tweenDisappear?.Kill();
      });
      
      _canvasGroup = canvasGroup;
      SetupBeforeOpen();
    }

    public void Appear(Action onStart = null, Action onEnd = null, float delay = 0, float duration = Duration)
    {  
      if (_canvasGroup == null)
      {
        onStart?.Invoke();
        onEnd?.Invoke();
        return;
      }

      _canvasGroup.blocksRaycasts = true;
      _canvasGroup.alpha = StartValue;
      _tweenAppear = _canvasGroup.DOFade(EndValue, duration).SetEase(Ease.Linear).SetDelay(delay);
      _tweenAppear.onComplete += () => onEnd?.Invoke();
      onStart?.Invoke();
    }

    public void Disappear(Action onStart = null, Action onEnd = null, float delay = 0, float duration = Duration)
    {
      if (_canvasGroup == null)
      {
        onStart?.Invoke();
        onEnd?.Invoke();
        return;
      }
      
      _canvasGroup.blocksRaycasts = false;
      _tweenDisappear = _canvasGroup.DOFade(StartValue, duration).SetEase(Ease.Linear);
      _tweenDisappear.onComplete += () => onEnd?.Invoke();
      onStart?.Invoke();
    }

    public void Clear()
    {
      _tweenAppear?.Kill();
      _tweenDisappear?.Kill();

      _tweenAppear = null;
      _tweenDisappear = null;
    }
    
    private void SetupBeforeOpen() => 
      _canvasGroup.alpha = 0;
  }
}