﻿using System;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Animation
{
  public class SlidePopup
  {
    private const float Duration = .45f;
    
    private readonly RectTransform _transform;
    
    private readonly float _yPosition;

    public SlidePopup(RectTransform transform)
    {
      _yPosition = Screen.height;
      _transform = transform;
      
      SetupBeforeOpen();
    }

    public void Open(Action onStart = null, Action onEnd = null, float endPosition = 0f, float delay = 0f)
    {
      _transform.localPosition = new Vector2(0, Screen.height);
      
      SetupBeforeOpen();

      _transform.DOAnchorPosY(endPosition, Duration).SetEase(Ease.Linear)
        .SetDelay(delay)
        .onComplete += () => onEnd?.Invoke();
      onStart?.Invoke();
    }

    public void Close(Action onStart = null, Action onEnd = null)
    {
      _transform.DOAnchorPosY(-_yPosition, Duration).SetEase(Ease.Linear).onComplete += () => onEnd?.Invoke();
      onStart?.Invoke();
    }

    private void SetupBeforeOpen() => 
      _transform.anchoredPosition = new Vector2( _transform.anchoredPosition.x, _yPosition);
  }
}